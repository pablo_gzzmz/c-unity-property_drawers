
using UnityEngine;
using UnityEditor;
using Gameplay;

/// <summary>
/// This editor script allows to use Base Game Elements' icon as their .asset preview.
/// </summary>

[CustomEditor(typeof(BaseGameElement), true)]
public class BaseGameElementEditor : Editor {

    public override Texture2D RenderStaticPreview(string assetPath, Object[] subAssets, int width, int height) {
        BaseGameElement actor = target as BaseGameElement;

        if (actor == null || actor.Icon == null)
            return null;

        Texture2D cache = new Texture2D(width, height);
        if (cache != null && actor.Icon.texture != null) {
            Texture2D preview = AssetPreview.GetAssetPreview(actor.Icon.texture);
            if (preview != null) {
                EditorUtility.CopySerialized(preview, cache);
            }
        }
        return cache != null ? cache : null;
    }
}