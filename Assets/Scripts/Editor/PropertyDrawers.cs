
using UnityEngine;
using UnityEditor;

/// <summary>
/// Some simple examples of property drawers.
/// They can be tested with the placeholder Scriptable Objects created for this project.
/// </summary>

namespace Gameplay {
    
    [CustomPropertyDrawer(typeof(Abilities))]
    public class AbilitiesDrawer : PropertyDrawer {

        private float heightMargin = 12;

        private float initialXPadding = 90;
        private float buttonSize = 20;
        private float objectRectHeight = 16;
        private float spacing = 26;

        private float buttonYPosFixSize = 2;

        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label) {

            position = EditorGUI.PrefixLabel(position, GUIUtility.GetControlID(FocusType.Passive), label);

            Rect rect = new Rect(initialXPadding, position.y-buttonYPosFixSize, buttonSize, position.height);

            SerializedProperty array = property.FindPropertyRelative("abilities");

            if (array.arraySize < Abilities.maximum) {
                rect.width = buttonSize;
                rect.height = buttonSize;
                if (GUI.Button(rect, "+")) {
                    array.arraySize++;
                }
            }

            float width = 200;

            rect.position = new Vector2(buttonSize*2, position.y + spacing);
            for (int i = 0; i < array.arraySize; i++) {
                rect.position = new Vector2(buttonSize*2, rect.position.y);
                rect.width = width;
                rect.height = objectRectHeight;
                EditorGUI.PropertyField(rect, array.GetArrayElementAtIndex(i), GUIContent.none);
                rect.position -= new Vector2(-width - 6, buttonYPosFixSize);
                rect.width = buttonSize;
                rect.height = buttonSize;
                if (GUI.Button(rect, "-")) {
                    array.DeleteArrayElementAtIndex(i);
                }
                rect.position += new Vector2(0, spacing);
            }
        }

        public override float GetPropertyHeight(SerializedProperty property, GUIContent label) {
            SerializedProperty array = property.FindPropertyRelative("abilities");

            return base.GetPropertyHeight(property, label) + spacing * array.arraySize + heightMargin;
        }
    }
    
    [CustomPropertyDrawer(typeof(Placement))]
    public class PlacementDrawer : PropertyDrawer {

        private float initialLabelMargin = 10;
        private float startXPadding = 90;

        private float inUseButtonSize = 20;
        private float rectSize = 20;
        private float spacing = 5;

        private Color centerRectColor     = new Color(0.1f, 0.6f, 0f);
        private Color emptyRectColor      = new Color(0.65f, 0.6f, 0.6f);
        private Color regularRectColor    = new Color(0f, 0.75f, 0.2f);
        private Color regularOutlineColor = new Color(0.4f, 0.4f, 0.4f);

        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label) {

            position.position += new Vector2(0, initialLabelMargin);

            SerializedProperty inUse = property.FindPropertyRelative("inUse");
            SerializedProperty size  = property.FindPropertyRelative("size");
            SerializedProperty xamount = size.FindPropertyRelative("x");
            SerializedProperty yamount = size.FindPropertyRelative("y");
            
            position = EditorGUI.PrefixLabel(position, GUIUtility.GetControlID(FocusType.Passive), label);

            Rect rect = new Rect(startXPadding, position.y - 2, inUseButtonSize, inUseButtonSize);
            
            // Draw inUse check button

            if (inUse.boolValue) {
                if (GUI.Button(rect, "✓")) {
                    inUse.boolValue = false;
                }
                // If the button has not been clicked (cancelled placement), draw
                else {
                    DrawPlacement(xamount, yamount, rect);
                }
            }
            else {
                // If the button has been clicked, placement has been activated
                if (GUI.Button(rect, "X")) {
                    inUse.boolValue = true;
                    
                    DrawPlacement(xamount, yamount, rect);
                }
            }
        }

        private void DrawPlacement(SerializedProperty xamount, SerializedProperty yamount, Rect rect) {
            if (xamount.intValue < 1) xamount.intValue = 1;
            if (yamount.intValue < 1) yamount.intValue = 1;

            // Initial margin
            rect.position += new Vector2(inUseButtonSize + spacing, 0);

            // Local rect
            Rect r = new Rect(rect);
            r.width  = rectSize;
            r.height = rectSize;

            float fullRectSize = rectSize + spacing;
            r.position += new Vector2(0, 0);

            // Draw selectable buttons alongside x
            for (int i = 1; i <= Placement.MaxSize; i++) {
                // If the value coincides with the x amount, it is blocked and a simple gray rect is drawn
                if (xamount.intValue == i) {
                    EditorGUI.DrawRect(r, Color.gray);
                    Rect r2 = r;
                    r2.position += new Vector2(spacing, 2);
                    EditorGUI.LabelField(r2, i.ToString());
                }
                // Otherwise, draw a button that can activate the index as the new x amount
                else
                if (GUI.Button(r, i.ToString())) {
                    xamount.intValue = i;
                }
                r.position += new Vector2(fullRectSize, 0);
            }

            // Go back to original position
            r.position = rect.position;
            
            r.position += new Vector2(-fullRectSize, fullRectSize);

            // Draw selectable buttons alongside y
            for (int i = 1; i <= Placement.MaxSize; i++) {
                if (yamount.intValue == i) {
                    EditorGUI.DrawRect(r, Color.gray);
                    Rect r2 = r;
                    r2.position += new Vector2(spacing, 2);
                    EditorGUI.LabelField(r2, i.ToString());
                }
                else
                if (GUI.Button(r, i.ToString())) {
                    yamount.intValue = i;
                }
                r.position += new Vector2(0, fullRectSize);
            }

            // Draw rects based on placement
            rect.position += new Vector2(0, fullRectSize);
            rect.width  = rectSize;
            rect.height = rectSize;

            for (int y = 0; y < Placement.MaxSize; y++) {
                for (int x = 0; x < Placement.MaxSize; x++) {
                    // Check if the current coordinate is notified as a placement spot
                    if (y < yamount.intValue && x < xamount.intValue) {
                        // R rect is used to paint an outline
                        r = rect;
                        r.width += 2;
                        r.height += 2;
                        r.position -= new Vector2(1, 1);

                        // Spot if center
                        if (y == Mathf.FloorToInt((yamount.intValue - 1) / 2) && x == Mathf.FloorToInt(xamount.intValue / 2)) {
                            EditorGUI.DrawRect(r, Color.black);
                            EditorGUI.DrawRect(rect, centerRectColor);
                        }
                        else {
                            EditorGUI.DrawRect(r, regularOutlineColor);
                            EditorGUI.DrawRect(rect, regularRectColor);
                        }
                    }
                    else {
                        EditorGUI.DrawRect(rect, emptyRectColor);
                    }
                    rect.position += new Vector2(fullRectSize, 0);
                }
                rect.position += new Vector2(-fullRectSize * Placement.MaxSize, fullRectSize);
            }
        }

        public override float GetPropertyHeight(SerializedProperty property, GUIContent label) {
            float h = base.GetPropertyHeight(property, label) + rectSize;

            if (property.FindPropertyRelative("inUse").boolValue) h += (rectSize + spacing) * Placement.MaxSize;
            
            return h;
        }
    }

    [CustomPropertyDrawer(typeof(Attack))]
    public class AttackDrawer : PropertyDrawer {
        private float startX = 30;
        private float xPadding = 90;
        private float ySpacing = 16;

        private float labelWidth = 100;
        private float fixedNumberWidth = 32;

        private float height;

        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label) {
            height = 0;

             EditorGUI.BeginProperty(position, label, property);

            position = EditorGUI.PrefixLabel(position, GUIUtility.GetControlID(FocusType.Passive), label);

            Rect rect = new Rect(startX, position.y + ySpacing, xPadding, ySpacing);

            SerializedProperty hasProjectile = property.FindPropertyRelative("hasProjectile");

            EditorGUI.LabelField(rect, "Has Projectile");
            rect.x += xPadding;
            EditorGUI.PropertyField(rect, hasProjectile, GUIContent.none);
            height += ySpacing;

            if (hasProjectile.boolValue) {
                rect.width = labelWidth;
                rect.x  = position.x;
                rect.y += ySpacing;
                height += ySpacing;
                EditorGUI.LabelField(rect, "Speed");
                rect.x += labelWidth;
                rect.width = fixedNumberWidth;
                EditorGUI.PropertyField(rect, property.FindPropertyRelative("speed"), GUIContent.none);
                rect.y += ySpacing;
                height += ySpacing;
                rect.width = labelWidth;
                rect.x  = position.x;
                EditorGUI.LabelField(rect, "Some value");
                rect.x += labelWidth;
                rect.width = fixedNumberWidth;
                EditorGUI.PropertyField(rect, property.FindPropertyRelative("someImportantFloat"), GUIContent.none);
                rect.y += ySpacing;
                height += ySpacing;
                rect.width = labelWidth;
                rect.x  = position.x;
                EditorGUI.LabelField(rect, "Some value");
                rect.x += labelWidth;
                rect.width = fixedNumberWidth;
                EditorGUI.PropertyField(rect, property.FindPropertyRelative("anotherImportantFloat"), GUIContent.none);
                rect.y += ySpacing;
                height += ySpacing;
                rect.width = labelWidth;
                rect.x  = position.x;
                EditorGUI.LabelField(rect, "Projectile");
                rect.x += labelWidth;
                rect.width = Screen.width - position.x - 120;
                EditorGUI.PropertyField(rect, property.FindPropertyRelative("attackGO"), GUIContent.none);
                rect.x = position.x;
                rect.y += ySpacing;
                height += ySpacing;
                EditorGUI.LabelField(rect, "Sound");
                rect.x += labelWidth;
                rect.width = Screen.width - position.x - 120;
                EditorGUI.PropertyField(rect, property.FindPropertyRelative("sound"), GUIContent.none);
            }

             EditorGUI.EndProperty();
        }

        public override float GetPropertyHeight(SerializedProperty property, GUIContent label) {
            return base.GetPropertyHeight(property, label) + height + 4;
        }
    }
}