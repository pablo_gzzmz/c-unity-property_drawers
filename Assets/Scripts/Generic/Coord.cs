
using UnityEngine;
using System;

[Serializable]
public struct Coord {
    public int x;
    public int y;

    public Coord(int _x, int _y) { x = _x; y = _y; }

    public Coord(Vector3 v) {
        x = (int)v.x;
        y = (int)v.z;
    }

    public static implicit operator Vector3(Coord coord) {
        Vector3 v = new Vector3(coord.x, 0, coord.y);

        return v;
    }

    public static implicit operator Coord(Vector3 v) {
        return new Coord(v);
    }

    public static Coord operator +(Coord coord1, Coord coord2) {
        return new Coord(coord1.x + coord2.x, coord1.y + coord2.y);
    }

    public static Coord operator -(Coord coord1, Coord coord2) {
        return new Coord(coord1.x - coord2.x, coord1.y - coord2.y);
    }

    public static bool operator ==(Coord coord1, Coord coord2) {
        return coord1.x == coord2.x && coord1.y == coord2.y;
    }

    public static bool operator !=(Coord coord1, Coord coord2) {
        return coord1.x != coord2.x || coord1.y != coord2.y;
    }

    public override bool Equals(object obj) {
        return base.Equals(obj);
    }

    public override int GetHashCode() {
        return base.GetHashCode();
    }

}