
using UnityEngine;
using System;
using System.Text;

namespace Gameplay {

    /// <summary>
    /// Base placeholder data holder class for an ability.
    /// </summary>

    [CreateAssetMenu(fileName = "Ability", menuName = "Database/Ability")]
    public class Ability : BaseGameElement {

    }

    // Struct for ability arrays

    [Serializable]
    public struct Abilities {
        public const int maximum = 12;

        [SerializeField]
        private Ability[] abilities;
        public  Ability[] AbilitiesArray { get { return abilities; } }

        public Abilities(Ability[] _abilities) {
            abilities = _abilities;
        }

        public Ability this[int key] {
            get {
                return abilities[key];
            }
            set {
                abilities[key] = value;
            }
        }

        /// <summary>
        /// Inserts an ability at the beggining of the array.
        /// </summary>
        public void Insert(Ability ability) {
            Ability[] temp = new Ability[abilities.Length+1];

            temp[0] = ability;

            for (int i = 1, n = 0; i <= abilities.Length; i++, n++) {
                temp[i] = abilities[n];
            }

            abilities = temp;
        }

        /// <summary>
        /// Adds an ability at the end of the array.
        /// </summary>
        public void Add(Ability ability) {
            Ability[] temp = new Ability[abilities.Length+1];

            temp[abilities.Length] = ability;

            for (int i = 0; i < abilities.Length; i++) {
                temp[i] = abilities[i];
            }

            abilities = temp;
        }

        public int Length { get { return abilities.Length; } }
    }
}
