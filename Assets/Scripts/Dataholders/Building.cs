
using UnityEngine;
using System;

namespace Gameplay {

    /// <summary>
    /// Placeholder for a scriptable object.
    /// </summary>

    [CreateAssetMenu(fileName = "Building", menuName = "Database/Building")]
    public class Building : Entity {

        [Space(10)]
        [Header("Building")]

        [Space(6)]
        [SerializeField]
        protected Placement placement;
        public    Placement Placement { get { return placement; } }

        [SerializeField]
        protected Attack attack;
        public    Attack Attack { get { return attack; } }    
    }
}
