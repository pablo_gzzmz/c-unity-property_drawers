
using UnityEngine;

namespace Gameplay {

    /// <summary>
    /// Abstract class for data serialization of any element existing in the gameplay context.
    /// </summary>
    
    public abstract class BaseGameElement : ScriptableObject {
        public string Name { get { return name; } }

        [SerializeField]
        protected Sprite icon = null;
        public    Sprite Icon { get { return icon; } }

        [Multiline]
        [SerializeField]
        protected string description = "Description.";
        public    string Description { get { return description; } }
    }
}
