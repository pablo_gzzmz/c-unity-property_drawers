
using UnityEngine;
using System.Collections.Generic;

namespace Gameplay {
    
        /// <summary>
        /// Example of an entity scriptable object
        /// </summary>

        public abstract class Entity : Ability {

        [Header("Entity")]
        [Space(12)]
        [SerializeField]
        protected Abilities abilities;
        public    Abilities Abilities { get { return abilities; } }
    }
}