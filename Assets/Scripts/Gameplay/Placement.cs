
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

/// <summary>
/// A placement is a matrix of coords.
/// </summary>

[Serializable]
public class Placement {
    public const int MaxSize = 4;

    public bool inUse;

    public Coord size;

    public bool[,] tiles;

    public Coord center;

    public Placement() {
        inUse = false;

        size.x = 1;
        size.y = 1;
    }

    public Placement(Placement other) {
        inUse = other.inUse;
        size = other.size;
        tiles = other.tiles;
        center = other.center;
    }

    public Placement(int _xamount, int _yamount) {
        if (_xamount > 0 || _yamount > 0)
            inUse = true;

        else inUse = false;

        size.x = _xamount;
        size.y = _yamount;

        SetTiles();
        SetCenter();
    }

    public void Set() {
        if (size.x > 0 || size.y > 0) {
            inUse = true;
        }
        else {
            inUse = false;
            return;
        }

        SetTiles();
        SetCenter();
    }

    private void SetCenter() {
        center.x = Mathf.FloorToInt(size.x / 2);
        center.y = Mathf.FloorToInt(size.y / 2);
    }

    private void SetTiles() {
        tiles = new bool[size.x, size.y];
        for (int x = 0; x < size.x; x++) {
            for (int y = 0; y < size.y; y++) {
                tiles[x, y] = true;
            }
        }
    }
}
