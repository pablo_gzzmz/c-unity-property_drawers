
using UnityEngine;
using System;

namespace Gameplay {

    /// <summary>
    /// Simple placeholder serializable class to depict attacks.
    /// </summary>

    [Serializable]
    public class Attack {
        [SerializeField]
        private bool hasProjectile = true;

        [SerializeField]
        private float speed = 1;

        [SerializeField]
        private float someImportantFloat = 1f;
        public  float SomeImportantFloat { get { return someImportantFloat; } }

        
        [SerializeField]
        private float anotherImportantFloat = 1f;
        public  float AnotherImportantFloat { get { return anotherImportantFloat; } }

        [SerializeField]
        private GameObject attackGO = null;
        public  GameObject AttackGO { get { return attackGO; } }

        [SerializeField]
        private AudioClip sound = null;
    }
}