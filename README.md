# README #
#
Author: Pablo González Martínez
#
pablogonzmartinez@gmail.com

Unity3D Engine
C#

This project showcases a simple implementation of property drawers to be used for ease of data serialization in Unity3D(C#).

# Google Docs doc#
https://docs.google.com/document/d/1WcEW7a-VRwZyqtvtIpX6sJ1xJkJ3ErPdqxf5xLJ5AAA/edit?usp=sharing

# Requirements #
The project has been created with Unity 2017.1.1f1 (64-bit).

# After-download guide: #
It contains no .exe, as the execution of the project itself ignoring the code is not very relevant. It should be opened in Unity using the parent folder containing ‘Assets’ and ‘Project Settings’ folders.

The project is organized in folders, and visualizing the result of the property drawers can be done via the Scriptable Object labelled as ‘Example’, found in Resources -> Database.

All the code is contained in the Scripts folder and has been created exclusively by the author.